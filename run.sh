#!/bin/bash

file_array=('Errazuriz.png' 'Marquez.png' 'PlayaAncha.png' 'SantiagoSeverin.png' 'Templeman.png' 'Montealegre.png')

for input_file in "${file_array[@]}"
  do
    python3 color_kmeans.py --image $input_file --clusters 8
    #python3 color_kmeans.py --image Errazuriz.png --clusters 8
done
